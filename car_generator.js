/**
 * Created by zhuk on 28.05.16.
 */

"use strict";

class CarGenerator {
    constructor(road){
       this.road = road;
        this.mean = 5000; // в секундах без ускорения
        this.deviant = 2000;
        var generator = this;

        var timeout =  Math.floor((Math.random()-Math.random())*this.deviant+this.mean)/time_speed;
        setTimeout(function(){
           generator.addCar();
        }, timeout)
    }

    addCar (){
        this.road.addCar();
        var generator = this;
        var timeout =  Math.floor((Math.random()-Math.random())*this.deviant+this.mean)/time_speed;
        setTimeout(function(){
            generator.addCar();
        }, timeout)
    }
}

module.exports = CarGenerator;