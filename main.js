/**
 * Created by zhuk on 28.02.16.
 */

var Road = require('./road.js');
var TrafficLight = require('./light.js');
var Generator = require('./car_generator.js');

need_debug = false;
time_speed = 10;
road_lenght = 175;
traffic_light_position = 140;
traffic_light_cycle = 120;
ratio = 0.6;

function main () {
    console.log("start…");
    l1 = new TrafficLight(traffic_light_cycle, ratio)
    r1 = new Road('NS', l1, traffic_light_position);
    r2 = new Road('SN', l1, traffic_light_position);
    r3 = new Road('WE', l1, traffic_light_position);
    r4 = new Road('EW', l1, traffic_light_position);
    generator1 =  new Generator(r1);
    generator2 =  new Generator(r2);
    generator3 =  new Generator(r3);
    generator4 =  new Generator(r4);

    setInterval(function(){r1.debugPrint()}, 1000/time_speed);
    // вклюить вывод сразу из всех дорог нельзя, так как они пытаются одновременно начать писать в консоль
    // нужно подумать просто что выводить в лог и выдить они раз посто сразу
    //setInterval(function(){r2.debugPrint()}, 1000/time_speed);
    //setInterval(function(){r3.debugPrint()}, 1000/time_speed);
    //setInterval(function(){r4.debugPrint()}, 1000/time_speed);

    //setTimeout(function(){r1.addCar()}, 3000/time_speed)
    //setInterval(function(){r1.addCar()}, 4000/time_speed)
    //setTimeout(function(){r1.setLength(170)}, 20000/time_speed)
}

main()