/**
 * Created by zhuk on 06.02.16.
 */

"use strict";

class Car  {
    constructor(road){
        this.arrive_time = new Date().getTime();
        this.current_speed = 17;
        this.road = road;
        this.id = '_' + Math.random().toString(36).substr(2, 9);
        this.x = 0;

        var car = this;

        this.intervalId = setInterval(function(){
            car.move()
        },1000/time_speed)
    }

    move(){
        //move car
        this.x = Math.round(this.x + this.current_speed);
        if(this.x > this.road.getLenght()) {
            this.road.removeCar(this.id, new Date().getTime()-this.arrive_time)
            clearInterval(this.intervalId);
        }

        //calculate next speed
        this.getDecision();
    }

    getSpeedByLight(distanceToTrafficLight){
        var lightState = this.road.getTrafficLightState();
        if ((lightState != 'green') && distanceToTrafficLight < 100){
            if (distanceToTrafficLight <= 2) {this.current_speed=0; return 0;}

            var timeToLight = distanceToTrafficLight / (this.current_speed);
            // если светофор уже мигает или желтый, то принимаем решение продолжать жвижение или тормозим
            // считаем время за которое проедем светофор
            if (lightState == 'flash_green' && timeToLight > 5) return -1 * (this.current_speed-0)/timeToLight
            if (lightState == 'yellow' && timeToLight > 2) return -1 * (this.current_speed-0)/timeToLight
            if (lightState == 'red' ) return -1 * (this.current_speed-0)/timeToLight
            if (need_debug) console.log('new speed', this.current_speed, 'm/s')
        }
        else {
            //если светофор зеленый и машинапервая или до светофора более 100 м, то можно ускоряться до 60км/ч
            return (17 - this.current_speed)/8
        }
    }

    getSpeedByCars(distance){
        if (distance <= 2) {this.current_speed=0; return 0;}
        if (distance <= 5) {this.current_speed=0.65; return 0;}
        if (distance > 5 ) this.current_speed=2;
        //зависимость дистанции от скорости
        var speedDistance = {
            '10': 5,
            '20': 10,
            '30': 15,
            '40': 20,
            '50': 25,
            '60': 30
        }
        var speedInKm = this.current_speed * 3600 / 1000;
        if (speedInKm<=10) return (distance - speedDistance['10'])/2
        if (speedInKm<=20) return (distance - speedDistance['20'])/2
        if (speedInKm<=30) return (distance - speedDistance['30'])/2
        if (speedInKm<=40) return (distance - speedDistance['40'])/2
        if (speedInKm<=50) return (distance - speedDistance['50'])/2
        if (speedInKm>50) return (distance - speedDistance['60'])/2
    }

    getDecision () {
        var distance = this.road.getDistanceToNextCar(this.x);
        var distanceToTrafficLight = this.road.getDistanceToTrafficLight(this.x);
        if (distanceToTrafficLight < distance){
            // значит машина первая к светофору – ориентируемся только на его состояние
           this.current_speed += this.getSpeedByLight(distanceToTrafficLight)
        }
        else {
            //если машина не первая, то в смотрим на впереди едущую машину прежде всего
            var speedIncrementByLight = this.getSpeedByLight(distanceToTrafficLight);
            var speedIncrementByCars = this.getSpeedByCars(distance);
            if (need_debug) console.log(this.current_speed, speedIncrementByLight, speedIncrementByCars)
            this.current_speed += (speedIncrementByLight < speedIncrementByCars ) ?  speedIncrementByLight : speedIncrementByCars
        }
    }

}
module.exports = Car;



