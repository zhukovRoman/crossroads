/**
 * Created by zhuk on 09.05.16.
 */


"use strict";

var Car = require('./car.js');

class Road  {
    constructor(direction, trafficLight, trafficLightX){
        if (need_debug) console.log('create road');
        this.cars = [];
        this.length = road_lenght;
        this.direction = direction;
        this.trafficLight = trafficLight;
        this.trafficLightX = trafficLightX;

        //статистика
        this.carsGets = 0;
        this.carsTimes = 0;
    }

    setLength (x){
        this.lenght = x;
    }
    removeCar(carId, carTime){
        this.cars.shift();
        this.carsGets ++;
        this.carsTimes += carTime*time_speed;
    }

    getLenght (){
       return this.length;
    }
    getDistanceToNextCar(car_x){
       var dist =  this.length-car_x;
        for (var i = 0; i<this.cars.length; i++){
            if (this.cars[i].x > car_x && this.cars[i].x-car_x < dist)
            dist = this.cars[i].x-car_x;
        }
        return dist;
    }

    getDistanceToTrafficLight (carX){
        return this.trafficLightX-carX;
    }

    getTrafficLightState (){
        return this.trafficLight.getState(this.direction)
    }

    addCar(){
        this.cars.push(new Car(this));
    }

    debugPrint(){
        var road = this;
        process.stdout.write(this.direction)
        function checkPoint(x){
            for (var i = 0; i<road.cars.length; i++){
                if (x==road.cars[i].x) return true;
            }
            return false;
        }
        for(var i = 0; i < road.length; i++){
            if(i==this.trafficLightX) {
                process.stdout.write(this.trafficLight.getStatePrint(this.direction))
            }
            else if (checkPoint(i)){
                process.stdout.write('@')
            }
            else  process.stdout.write('.')

        }
        console.log('')
        console.log('машин проехало = ',this.carsGets,
            'общее время проезда всех машин =', this.carsTimes,
            'среднее время проезда всех машин =', this.carsTimes/this.carsGets
        )
    }

}
module.exports = Road;