/**
 * Created by zhuk on 21.05.16.
 */

"use strict";
//var events = require('events');
//var util = require('util');
class TrafficLight  {
  constructor (cycleDuration, ratio){
     this.states = {
         NS: 'red',
         WE: 'red'
     };
     this.yellow_time = 3 * 1000 / time_speed;
     this.flash_green_time = 5 * 1000 / time_speed;
     this.ns_time = cycleDuration * 1000 * ratio / time_speed;
     this.we_time = cycleDuration * 1000 * (1 -ratio) / time_speed;
     console.log('Новый светофор с временем цикла = ', cycleDuration, 'отношения времени = ', ratio)
     this.switchToWE()
  }

    switchToNS (){
        var self = this;
        self.states.WE = 'flash_green';
        setTimeout(function(){
            self.states.WE = 'yellow';
        },self.flash_green_time);
        setTimeout(function(){
            self.states.WE='red';
            self.states.NS='green';
        }, self.yellow_time+self.flash_green_time);
        setTimeout(function(){
          self.switchToWE()
        },self.ns_time)
    }

    switchToWE (){
        var self = this
        self.states.NS = 'flash_green';
        setTimeout(function(){
            self.states.NS = 'yellow';
        },self.flash_green_time);
        setTimeout(function(){
            self.states.NS='red';
            self.states.WE='green';
        }, self.yellow_time+self.flash_green_time);
        setTimeout(function(){
            self.switchToNS()
        },self.we_time)
    }

    debugPrint () {
        console.log(this.states)
    }

    getState (direction){
        return this.states[direction]
    }

    getStatePrint(direction){
        var state = this.states[direction]
        if (state=='red') return '|';
        if (state=='yellow') return '/';
        if (state=='green') return '-';
        if (state=='flash_green') return '*';
    }
}

module.exports = TrafficLight;

